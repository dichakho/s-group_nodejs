const {check, validationResult} = require('express-validator/check');
const checkValidateArticle = [
    
    check('title').trim().not().isEmpty().withMessage('Title không được để trống'),
    check('content').not().isEmpty().withMessage('Content không được để trống'),
    check('author').trim().not().isEmpty().withMessage('Author không được để trống')

    ];

const checkValidateCategory = [
    check('category').trim().not().isEmpty().withMessage('Category không được bỏ trống')
    ];
    module.exports = {
        checkValidateArticle,
        checkValidateCategory
    }