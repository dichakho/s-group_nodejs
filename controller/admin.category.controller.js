const Category = require('../models/blog/category.model');
const {check, validationResult} = require('express-validator/check');

const getAddCategory = (req,res) => {
    if(!req.session.Users) res.redirect('/admin/login');
    else {
        res.render('admin/add_category');
    }
};

const postAddCategory = (req, res) => {
    const data = req.body;
    const errors = validationResult(req); 
    if(!errors.isEmpty()) {
        let failArr = errors.array();
        console.log(failArr);
        let failMsg = new Object();
        for( let i = 0; i < failArr.length; i++) {
            failMsg[failArr[i].param] = failArr[i].msg;
        }
        console.log(failMsg);
        return res.render('admin/add_category',{ failMsg });
    } else {
        const listCategory = new Category({
            categoryName: data.category
        });

        listCategory.save((err,result) => {
            if(err) {
                console.log('Add fail'); 
                return;
            } 
            console.log("Add success");
            res.render('admin/add_category',{
                successMsg:'Add success'
            });
        });
    }
};

const manageCategory = (req,res) => {
    if(!req.session.Users) res.redirect('/admin/login');
    else {
        Category.find().select('categoryName createdAt updatedAt _id').sort( {createdAt:-1}).exec((err,Cate) => {
            if(err) {
                console.log("Error: ",err);
                return;
            } 
            res.render('admin/manage_category', {
                 Cate 
            });
        });
    }    
};



module.exports = {
    getAddCategory,
    postAddCategory,
    manageCategory
}