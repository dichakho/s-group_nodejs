const {check, validationResult} = require('express-validator/check');
const bcrypt = require('bcryptjs');
const User = require('../models/user/user.model');

const registerUserController = (req, res) =>{
    const {
        username,
        password,
        email,
    } = req.body;           //trong cái body của req
    const cUser = new User({
        username,
        password,
        email,
    });

    cUser.save((err, user) => {
        if(err){
            console.log(err);
            return;
        }
        console.log(user);
        res.redirect('/admin')
    });
};

const postLoginUserController = (req, res) => {
    const{
        username,
        password,
    } = req.body;

    User.findOne({
        username : username,
    }, (err, user) => {
        if(err){
            console.log(err);
            return;
        };

        if(user){
            if (bcrypt.compareSync(password, user.password)){
                console.log(user);
                req.session.Users = {
                    id: user._id,
                    username: user.username,
                    avatar : user.avatar
                };
                console.log(req.session);
                req.flash('success', 'Login succesfully!')
                res.redirect('/admin');
            } else {
                // console.log('Pass không chính xác');   
                req.flash('danger', 'Pass không chính xác');
                res.redirect('/admin/login'); 
            }
        } else {
            // console.log('Username không chính xác');
            req.flash('danger', 'Username không chính xác');
            res.redirect('/admin/login'); 
 
        }

    });
};

const getLoginUserController = (req, res) =>{
    if(req.session.Users) res.render('/admin')
    else res.render('admin/login');
};

const logoutController = (req, res)=>{
    req.session.destroy((err)=>{
        if(err){
            console.log(err);
            return;
        }
        res.redirect('/admin/login');
    });
};

module.exports = {
    registerUserController,
    postLoginUserController,
    getLoginUserController,
    logoutController
}