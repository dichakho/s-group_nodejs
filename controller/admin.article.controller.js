const {check, validationResult} = require('express-validator/check');
const session = require('express-session');
const Add = require('../models/blog/add.model');
const Category = require('../models/blog/category.model');

const middlewareModify = (req, res, next) => {
    const articleId = req.params.id;
    const userId = req.session.Users.id;
    Add
        .findById(articleId)
        .exec((err, article) => {
            if (err) {
                return next(err);
            };
            if (!article) return next(new Error('Article not found'));
            if (article.author !== userId) {
                req.flash('danger', 'Access denied')
                return res.redirect('/admin/article/manage/1');
            };
            next();
        });

};


const postAddArticle = async (req,res) => {
    const {
        title,
        author,
        description,
        content,
        category
    } = req.body;

    const errors = validationResult(req) ;
    if(!errors.isEmpty()) {
        let errorArr = errors.array();
        console.log(errorArr);
        let errorMsg = new Object();
        
        for(i = 0; i < errorArr.length; i++) {
            errorMsg[errorArr[i].param] = errorArr[i].msg;
        }
        let addCate = await Category.find().select('categoryName _id');
        console.log(errorMsg);
        return res.render('admin/add',{
            title, description, content, author, errorMsg, addCate
        });

    } else {
       
        const Adds = new Add({
            title,
            author: req.session.Users.id,
            description,
            content,
            category
        });
    
        Adds.save((err,add) => {
            if(err) {
                console.log(err);
                return;
            }
            console.log('success', add);
            
            res.redirect('/admin/article/manage');
        });    
    }
};

const getAddArticle = (req,res) => {
    if(req.session.Users == undefined) res.redirect('/admin/login');
    else {
        Category
        .find()
        .select('_id categoryName')
        .sort({ createdAt: -1 })
        .exec((err,addCate) => {
            if(err) {
                console.log("Error: ",err);
                return;
            } 
            res.render('admin/add',{
                addCate
            });
            console.log(addCate);
    
        });
    }
};

const manageArticle = (req, res, next) => {
    const perPage = 3;
    
    const page = req.params.page || 1; 
    console.log('hello', req.params);
    Add
        .find({})
        .populate('author','username')
        .populate('category','categoryName')
        .sort({createdAt: -1})
        .select('title description createdAt category updatedAt author url ')
        .skip((perPage * page) - perPage) 
        .limit(perPage)
        .exec((err, Adds) => {
            Add.count().exec((err, count) => {
                if (err){
                    console.log(err);
                    return;
                }
                res.render('admin/manage', {
                    Adds,
                    current: page,
                    pages: Math.ceil(count / perPage)
                });
            });
        });
};

const getManageAuthArticle = (req, res) => {
    console.log('hello', req.session.Users);
        const authId = req.session.Users.id;
        console.log(authId);
        Add
        .find({ author : authId })
        .populate('author', 'username')
        .populate('category','categoryName')
        .sort({
            updatedAt : -1,
            createdAt : -1
        })
        .select('title description content createdAt updatedAt author url')
        .exec((err, authArticle) => {
            if (err) {
                console.log(err);
                return;
            }
            res.render('admin/manageAuthArticle', {
                authArticle
            });
        });
    
};

const deleteArticle =(req,res)=>{
    Add.deleteOne({ _id: req.params.id},(err)=>{
        if(err){
            console.log(err);
            req.flash('danger','Delete article failed');
            return res.redirect('/admin/article/add');
        }
        
        req.flash('success','Delete successfully');
        res.redirect('/admin/article/manage');
    });
};

const getArticle = (req,res)=>{
    Add.findById(req.params.id)
    .populate('category','categoryName')
    .exec((err,article)=>{
        if(err){
            console.log(err);
            return;
        };
        if(!article){
            req.flash('danger','Can not found');
            return res.redirect('/admin/article/manage');
        };
        Category
        .find()
        .exec((err, cateList) => {
            if(err){
                console.log(err);
                return;
            }
            res.render('admin/edit', {
                cateList,
                article
            })
        })
    });
};

const editArticle = (req,res)=>{
    const{
        title,
        description,
        content,
        category
       
    } = req.body;
    const {id} =req.params;
    Add.findById({ _id: id},(err,article)=>{
        if(err){
            console.log(err);
            return;
        };
        console.log(article);
        if(!article){
            req.flash('danger','Can not found');
            return res.redirect('/admin/article/manage');
        };
        article.title = title;
        article.description = description;
        article.content = content;
        article.category = category;
        article.save((err, article)=>{
            if(err){
                console.log('>>>ERROR: ',err);
                req.flash('danger','Edit fail');
                return res.redirect('/admin/article/edit/${id}');
            }
            console.log('>>>Article: ', article);
            res.redirect('/admin/article/manage-author-article');
        });

    });
};


module.exports = {
    middlewareModify,
    postAddArticle,
    getAddArticle,
    manageArticle,
    deleteArticle,
    getArticle,
    editArticle,
    getManageAuthArticle
}
