const Add = require('../models/blog/add.model');

const getArticles = (req, res, next) => {
    Add
        .find()
        .select('-_id -createdAt -content -__v')
        .sort({
            updatedAt: -1
        })
        .populate('author', 'username')
        .exec((err, articles) => {
            if (err) {
                const error = new Error('Server Error');
                error.status = 500;
                return next(error);
            };
            console.log(articles);
            res.render(('client/index'), {
                articles,
                img: 'home',
                title: 'Clean Blog',
                note: 'A Blog Them by Start Boostrap'
            });
        });
};

const getArticleByUrl = (req, res, next) => {
    const url = req.params.url;
    Add
        .findOne({url})
        .select('-_id -createdAt -__v')
        .populate('author', 'username')
        .exec((err, article) => {
            if (err) {
                const error = new Error('Server Error');
                error.status = 500;
                return next(error);
            };
            if (!article) {
                const error = new Error('Not found');
                error.status = 404;
                return next(error);
            }
            res.render(('client/post'), {
                article,
                img: 'home',
                title: 'Clean Blog',
                note: 'A Blog Them by Start Boostrap'
            });
        });
};
module.exports = {
    getArticles,
    getArticleByUrl
};