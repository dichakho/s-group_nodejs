const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const Schema = mongoose.Schema;
const userSchema = new Schema({      //tạo bảng trong database
    username:{
        type: String,
        unique: true,
        lowercase:true,
        require: true,
    },
    email:{
        type: String,
        unique: true,
        lowercase: true,
        trim: true,
        required: true,
        
    },
    password:{
        type: String,
        required: true,

    },
    avatar: {
        type: String,
        default: 'https://images.vexels.com/media/users/3/129733/isolated/preview/a558682b158debb6d6f49d07d854f99f-casual-male-avatar-silhouette-by-vexels.png',
    }
},{
    timestamps: true,
});
userSchema.pre('save', function (next) {          //trước khi save thì làm bước mã hóa pwd ở dưới
    const user = this;
    if(!user.isModified('password')) return next();

    bcrypt.genSalt(10, (err, salt)=>{
        bcrypt.hash(user.password, salt, (err, hash)=>{
            if(err) return next();

            user.password = hash;
            next();
        })
    })
})

module.exports = mongoose.model('users', userSchema);
