const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const categorySchema = new Schema({
    categoryName: {
        type: String,
        require: true,
        unique : true
    }
},{
    timestamps:true
});

module.exports = mongoose.model('categories',categorySchema);