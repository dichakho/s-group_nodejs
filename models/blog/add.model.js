const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const getSlug = require('speakingurl');
const addSchema = new Schema({
    title:{
        type : String,
        required : true,
    },
    description:{
        type : String,
    },
    
    content:{
        type : String,
        required : true,
    },
    author:{
        type : Schema.Types.ObjectId,
        ref : 'users',
    },
    category:{
        type : Schema.Types.ObjectId, 
        ref : 'categories',
        required : true,
    },
    url:{
        type:String,
        trim:true,
    }
},{
    timestamps : true,
});

addSchema.pre('save', function (next) {
    const article = this;
    if (!article.isModified('title')) return next();
    const url = getSlug(article.title, {
        lang: 'vn',
    });
    const month = article.updatedAt.getMonth() + 1;
    const time = "" + article.updatedAt.getFullYear() + "0" + month + "0" + article.updatedAt.getDate() + article.updatedAt.getHours() + article.updatedAt.getMinutes() + article.updatedAt.getSeconds();
    article.url = url + time;
    
    console.log(url);
    next();
});
module.exports = mongoose.model('add', addSchema);