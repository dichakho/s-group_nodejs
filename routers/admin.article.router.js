const express = require('express');
const router = express.Router();

const {
    checkValidateArticle
} = require('../config/validate');

const {
    middlewareModify,
    postAddArticle,
    getAddArticle,
    manageArticle,
    deleteArticle,
    getArticle,
    editArticle,
    getManageAuthArticle
} = require('../controller/admin.article.controller');

router.use(['/delete/:id','/edit/:id'], middlewareModify);


router.route('/add')
    .get((req, res) => {
        getAddArticle(req, res);
    })
    .post(checkValidateArticle, (req, res) => {
        postAddArticle(req, res);
    });

router.get('/manage/:page',  manageArticle);

router.get('/delete/:id',(req, res)=>{
    req.params.id
    deleteArticle(req, res);
});

router.route('/edit/:id')
    .get((req, res)=>{
        getArticle(req, res);
    })
    .post((req, res)=>{
        editArticle(req, res);
    });

router.get('/manage', (req, res) => {
    manageArticle(req, res);
});

router.get('/manage-author-article', (req, res) => {
    getManageAuthArticle(req, res);
});
module.exports = router;