const express = require('express');
const session = require('express-session');
const router = express.Router();

const adminAuthorRouter = require('./admin.author.router');
const adminArticleRouter = require('./admin.article.router');
const adminCategoryRouter = require('./admin.category.router');

router.use('/', adminAuthorRouter);
router.use('/article', adminArticleRouter);
router.use('/category', adminCategoryRouter);

router.get('/manage_category' ,);

module.exports = router;