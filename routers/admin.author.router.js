const express = require('express');
const router = express.Router();
const {
    registerUserController,
    postLoginUserController,
    getLoginUserController,
    logoutController
} = require('../controller/admin.author.controller');

router.use((req, res, next) => {
    res.locals.flash_messages = req.session.flash;
    res.locals.Users = req.session.Users;
    delete req.session.flash;
    next();
});

router.use(['/login', '/register'], (req, res, next) => {
    if (req.session.user) {
        return res.redirect('/admin');
    }
    return next();
});
router.post('/register', (req, res) => {
    registerUserController(req, res);
});

router.route('/login')
    .get((req, res) => {
        getLoginUserController(req, res);
    })
    .post((req, res) => {
        postLoginUserController(req, res);
    })

router.use((req, res, next) => {
    if(!req.session.Users) return res.redirect('/admin/login');  
    next();
});

router.get('/' , (req, res) =>{
    if(!req.session.Users) res.redirect('/admin/login');
    else {
        console.log(req.session.Users);
        res.render('admin/index');
    }
});

router.get('/logout', (req, res) => {
    logoutController(req, res);
})
module.exports = router;