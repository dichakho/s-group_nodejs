const express = require('express');
const router = express.Router();

const {
    checkValidateCategory
} = require('../config/validate');

const {
    getAddCategory,
    postAddCategory,
    manageCategory
} = require('../controller/admin.category.controller');

router.route('/add_category')
    .get((req, res) => {
        getAddCategory(req, res);
    })
    .post(checkValidateCategory, (req, res) => {
        postAddCategory(req, res);
    })
router.get('/manage_category', (req, res) => {
    manageCategory(req, res);
})

module.exports = router;